# Documentation pour <b class="frama">Fram</b><b class="vert">agenda</b>

[<b class="frama">Fram</b><b class="vert">agenda</b>](https://framagenda.org) est un service libre proposé par [<b class="frama">Frama</b><b class="soft">soft</b>](https://framasoft.org) dans le cadre de sa campagne [**Dégooglisons Internet**](https://degooglisons-internet.org/).

Il s'agit d'une application de gestion d'**agendas**, de **contacts** et de **tâches**. <b class="frama">Fram</b><b class="vert">agenda</b> est basé sur le logiciel libre [NextCloud](https://nextcloud.com/) et sur plusieurs de ses applications.

### Interface

#### [Inscription & Connexion](Inscription-Connexion.md)
* S'inscrire sur Framagenda
* Se connecter à Framagenda
* Utiliser l'authentification en deux étapes (TOTP)
* Utiliser des mots de passe d'application

#### [Agenda](Interface-Agenda.md)
* Créer et modifier un calendrier
* Créer et modifier un événement
* Partager un événement avec d'autres utilisateurs de Framagenda
* Partager un événement par lien public
* Ajouter un abonnement

#### [Contacts](Interface-Contacts.md)
* Créer et modifier un contact
* Gérer ses carnets d'adresses
* Importer et exporter des contacts
* Partage des contacts

#### [Tâches](Interface-Tasks.md)
* Créer, modifier et valider une tâche.
* Créer des sous-tâches

### Synchronisation avec un client

#### Bureau
* [Thunderbird](Synchronisation/Thunderbird.md) (PC, MacOS, GNU/Linux, \*BSD)
* [Apple Calendar](Synchronisation/ical.md) (MacOS X)
* [Gnome](Synchronisation/Gnome.md) avec Evolution, Agenda et Contacts (GNU/Linux, \*BSD)

#### Mobile
* [DAVDroid](Synchronisation/Android.md) (Android)
* [iOS](Synchronisation/iOS.md) (paramètres internes)
* [Windows Phone](Synchronisation/WindowsPhone.md) (paramètres internes)
* [Ubuntu Touch](Synchronisation/UbuntuTouch.md) (paramètres internes)

### [FAQ](FAQ.md)
Votre question se trouve peut-être ici !
